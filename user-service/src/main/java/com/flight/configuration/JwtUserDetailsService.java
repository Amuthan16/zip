package com.flight.configuration;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.flight.repository.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		if ("admin".equals(username)) {
//			return new User("admin","{noop}admin",
//					new ArrayList<>());
//		}
		
		List<com.flight.entity.User> userList=userRepository.findByEmail(username);
		
		
		
		if (userList.size()>0) {
			com.flight.entity.User userVal=userList.get(0);
			String userEmail=userVal.getEmail();
//			String role=userVal.getRole();
			String passwd=passwordEncoder.encode(userVal.getPassword());
//			List<String>roleList=new ArrayList<>();
//			roleList.add(userVal.getRole());
//			return new User("demo", "$2a$10$HpRDgXNLCa22nW4eCQuETuB4qOu02.Lln/qyugx3Kw3jtP.Xy7JvS", new ArrayList<>());
			return new User(userEmail, passwd, new ArrayList<>());
		}
		else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
}
