package com.flight.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flight.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

//	@Query("SELECT * FROM User u WHERE u.email = :email")
	List<User> findByEmail(String email);

	List<User> findByEmailAndPassword(String email,String password);

}
