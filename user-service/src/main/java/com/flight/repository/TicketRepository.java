package com.flight.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flight.entity.TicketDetail;

@Repository
public interface TicketRepository extends JpaRepository<TicketDetail, Long>{

	List<TicketDetail> findByEmail(String email);

	List<TicketDetail> findByEmailAndStatus(String email, String status);

	List<TicketDetail> findByFlightId(long parseLong);

}
