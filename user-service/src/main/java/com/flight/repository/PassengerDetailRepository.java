package com.flight.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flight.entity.PassengerDetail;

@Repository
public interface PassengerDetailRepository extends JpaRepository<PassengerDetail, Long> {

}
