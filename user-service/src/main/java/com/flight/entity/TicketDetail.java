package com.flight.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ticket_detail")
public class TicketDetail {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String email;
    private Long flightId;
    private String flightName;
    private String dateOfJourney;
    private Long price;
    private String status;
    @Column(name="passengerDetail",nullable = true)
    @OneToMany(cascade=CascadeType.ALL)
    private List<PassengerDetail> passengerDetail;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the flightId
	 */
	public Long getFlightId() {
		return flightId;
	}
	/**
	 * @param flightId the flightId to set
	 */
	public void setFlightId(Long flightId) {
		this.flightId = flightId;
	}
	/**
	 * @return the flightName
	 */
	public String getFlightName() {
		return flightName;
	}
	/**
	 * @param flightName the flightName to set
	 */
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	/**
	 * @return the dateOfJourney
	 */
	public String getDateOfJourney() {
		return dateOfJourney;
	}
	/**
	 * @param dateOfJourney the dateOfJourney to set
	 */
	public void setDateOfJourney(String dateOfJourney) {
		this.dateOfJourney = dateOfJourney;
	}
	/**
	 * @return the price
	 */
	public Long getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Long price) {
		this.price = price;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the passengerDetail
	 */
	public List<PassengerDetail> getPassengerDetail() {
		return passengerDetail;
	}
	/**
	 * @param passengerDetail the passengerDetail to set
	 */
	public void setPassengerDetail(List<PassengerDetail> passengerDetail) {
		this.passengerDetail = passengerDetail;
	}
	/**
	 * @param id
	 * @param email
	 * @param flightId
	 * @param flightName
	 * @param dateOfJourney
	 * @param price
	 * @param status
	 * @param passengerDetail
	 */
	public TicketDetail(Long id, String email, Long flightId, String flightName, String dateOfJourney, Long price,
			String status, List<PassengerDetail> passengerDetail) {
		super();
		this.id = id;
		this.email = email;
		this.flightId = flightId;
		this.flightName = flightName;
		this.dateOfJourney = dateOfJourney;
		this.price = price;
		this.status = status;
		this.passengerDetail = passengerDetail;
	}
    
    public TicketDetail() {
    	
    }
}
