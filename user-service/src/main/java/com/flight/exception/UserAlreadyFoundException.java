package com.flight.exception;

public class UserAlreadyFoundException extends Exception{

	public UserAlreadyFoundException() {}
	public UserAlreadyFoundException(String m) {
		super(m);
	}
	public UserAlreadyFoundException(Exception e) {
		super(e);
	}
	public UserAlreadyFoundException(String m, Exception e) {
		super(m, e);
	}
}
