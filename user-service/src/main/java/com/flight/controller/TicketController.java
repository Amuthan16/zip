package com.flight.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flight.entity.TicketDetail;
import com.flight.exception.TicketNotFoundException;
import com.flight.service.TicketService;

@RestController
@CrossOrigin
public class TicketController {
	
	@Autowired
	private TicketService ticketService; 
	
	@CrossOrigin
	@GetMapping("/ticket")
	public ResponseEntity<List<TicketDetail>> getTicket(@RequestParam String email) throws TicketNotFoundException {
		
		return new ResponseEntity<List<TicketDetail>>(ticketService.getTicketByEmail(email),HttpStatus.OK);
		
	}
	
	@CrossOrigin
	@GetMapping("/ticketByStatus")
	public ResponseEntity<List<TicketDetail>> getTicketByStatus(@RequestParam String email, @RequestParam String status)  throws TicketNotFoundException{
		
		return new ResponseEntity<List<TicketDetail>>(ticketService.getTicketByEmailAndStatus(email,status),HttpStatus.OK);
		
	}
	
	@CrossOrigin
	@PostMapping("/ticket")
	public ResponseEntity<TicketDetail> createTicket(@RequestBody TicketDetail ticket) {
		
		return new  ResponseEntity<TicketDetail>(ticketService.saveTicket(ticket),HttpStatus.OK);
		
	}
	
	@PutMapping("/ticket/{id}")
	public ResponseEntity<TicketDetail> updateTicket(@RequestBody TicketDetail ticket) throws TicketNotFoundException {
		
		return new ResponseEntity<TicketDetail>(ticketService.updateTicket(ticket),HttpStatus.OK);
		
	}
	
	

	
}
