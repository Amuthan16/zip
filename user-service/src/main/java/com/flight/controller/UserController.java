package com.flight.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flight.entity.User;
import com.flight.exception.UserAlreadyFoundException;
import com.flight.service.UserService;


@RestController
@CrossOrigin("*")
public class UserController {
	
	@Autowired
	private  UserService userService;
	
	@GetMapping("/register")
	public  ResponseEntity<User> getUser(@RequestParam String email,@RequestParam(value = "password", required = false) String password) {
		System.out.println("asdasdad"+email+"sdada"+password);
		User userVal= userService.getUserByEmail(email,password);
		return new ResponseEntity<>(userVal, HttpStatus.OK);
	}

	@PostMapping("/register")
	public ResponseEntity<User> createUser(@RequestBody User user) throws UserAlreadyFoundException{
		
		User userVal=userService.saveUser(user);
		return new ResponseEntity<>(userVal, HttpStatus.CREATED);
	}
	

}
