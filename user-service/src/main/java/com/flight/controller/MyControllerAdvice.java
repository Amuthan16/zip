package com.flight.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.flight.exception.TicketNotFoundException;
import com.flight.exception.UserAlreadyFoundException;
import com.flight.model.ExceptionMessage;

@ControllerAdvice
public class MyControllerAdvice {

	@ExceptionHandler(UserAlreadyFoundException.class)
	public ResponseEntity<ExceptionMessage> handleUserAlreadyFoundExceptionException(UserAlreadyFoundException e) {
		ExceptionMessage obj = new ExceptionMessage(e.getMessage(), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<ExceptionMessage>(obj, HttpStatus.OK);
	}
	
	@ExceptionHandler(TicketNotFoundException.class)
	public ResponseEntity<ExceptionMessage> handleTicketNotFoundExceptionException(TicketNotFoundException e) {
		ExceptionMessage obj = new ExceptionMessage(e.getMessage(), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<ExceptionMessage>(obj, HttpStatus.OK);
	}
}
