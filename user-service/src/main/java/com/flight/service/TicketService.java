package com.flight.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.flight.entity.TicketDetail;
import com.flight.exception.TicketNotFoundException;
import com.flight.model.Airline;
import com.flight.repository.TicketRepository;

@Service
public class TicketService {
	
	
	@Autowired
	private TicketRepository ticketRepository;
			

	public List<TicketDetail> getTicketByEmail(String email) throws TicketNotFoundException {
		// TODO Auto-generated method stub
		List<TicketDetail> ticketDetail=ticketRepository.findByEmail(email);
		if(ticketDetail.size()>0) {
			return ticketDetail;
		}else {
			throw new TicketNotFoundException("No ticket found with this email "+email);
		}
		
	}

	public List<TicketDetail> getTicketByEmailAndStatus(String email, String status) throws TicketNotFoundException {
		// TODO Auto-generated method stub
		List<TicketDetail> ticketDetail=ticketRepository.findByEmailAndStatus(email,status);
		if(ticketDetail.size()>0) {
			return ticketDetail;
		}
		else {
			throw new TicketNotFoundException("No ticket found with this email "+email);
		}
	}

	public TicketDetail saveTicket(TicketDetail ticket) {
		// TODO Auto-generated method stub
		
		TicketDetail ticketDetail=ticketRepository.save(ticket);
		updateSeatNumber(ticketDetail);
		
		return ticketDetail;
	}

	public void updateSeatNumber(TicketDetail ticketDetail) {
		// TODO Auto-generated method stub
		String url = "http://ec2-13-58-180-214.us-east-2.compute.amazonaws.com:8989/api/admin/flight/"+ticketDetail.getFlightId();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("Accept", "application/json");
		HttpEntity<Airline> entity = new HttpEntity<Airline>(headers);
		 RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Airline> res = restTemplate.exchange(
				url, 
				HttpMethod.GET, 
				entity, 
				new ParameterizedTypeReference<Airline>() {}
			);
		Airline airline=res.getBody();
		Long seats=airline.getTotalNoOfSeats();
		Long calcSeats=seats-ticketDetail.getPassengerDetail().size();
		airline.setTotalNoOfSeats(calcSeats);

		HttpHeaders headersPost = new HttpHeaders();
		headersPost.add("Content-Type", "application/json");
		headersPost.add("Accept", "application/json");
		HttpEntity<Airline> entityAirline = new HttpEntity<Airline>(airline, headersPost);
		restTemplate.exchange(url, HttpMethod.PUT, entityAirline, new ParameterizedTypeReference<Airline>() {});
	}

	public TicketDetail updateTicket(TicketDetail ticket) throws TicketNotFoundException {
		// TODO Auto-generated method stub
		Optional<TicketDetail> ticketDetail=ticketRepository.findById(ticket.getId());
		if(ticketDetail.isPresent()) {
			return ticketRepository.save(ticket);
		}else {
			throw new TicketNotFoundException("No ticket found with this id "+ticket.getId());
		}
		
	}
	
//	@KafkaListener(topics = "flightTopic", groupId="group_id", containerFactory = "userKafkaListenerFactory")
//	public void consumeJson(String value) {
//	    Long flightId= Long.parseLong(value.replace("\"", ""));
//	    List<TicketDetail>ticketDetailList=ticketRepository.findByFlightId(flightId);
//	    for(TicketDetail ticketDetail:ticketDetailList) {
//	    	ticketDetail.setStatus("cancelled");
//	    }
//	    ticketRepository.saveAll(ticketDetailList);
//	}

}
