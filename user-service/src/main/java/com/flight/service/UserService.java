package com.flight.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flight.entity.User;
import com.flight.exception.UserAlreadyFoundException;
import com.flight.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public User getUserByEmail(String email, String password) {
		// TODO Auto-generated method stub
		if(password==null) {
			return userRepository.findByEmail(email).get(0);
		}else {
			return userRepository.findByEmailAndPassword(email,password).get(0);
		}
		
	}

	public User saveUser(User user) throws UserAlreadyFoundException {
		// TODO Auto-generated method stub
		if(userRepository.findByEmail(user.getEmail()).isEmpty()) {
			return userRepository.save(user);
		}else {
			throw new UserAlreadyFoundException("Already User exists with this email:"+user.getEmail());
		}
		
	}

}
