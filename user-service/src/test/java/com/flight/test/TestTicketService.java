package com.flight.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.flight.entity.PassengerDetail;
import com.flight.entity.TicketDetail;
import com.flight.exception.TicketNotFoundException;
import com.flight.repository.TicketRepository;
import com.flight.service.TicketService;

@SpringBootTest
public class TestTicketService {

	@Autowired
	TicketService ticketService;

	@MockBean
	TicketRepository ticketRepository;

	@Test
	public void testGetAllTicketByEmail() throws TicketNotFoundException{
		List<TicketDetail> ticketDetailList = new ArrayList<TicketDetail>();
		TicketDetail ticketDetail=new TicketDetail();
		ticketDetail.setId((long) 1);
		ticketDetail.setEmail("user@gmail.com");
		ticketDetail.setFlightId((long) 58);
		ticketDetail.setFlightName("AirAsia");
		ticketDetail.setPrice((long) 4300);
		ticketDetail.setStatus("active");
		ticketDetail.setDateOfJourney("20-07-2021");
		PassengerDetail passengerDetail=new PassengerDetail();
		passengerDetail.setAge((long) 25);
		passengerDetail.setGender("male");
		passengerDetail.setId((long) 1);
		passengerDetail.setMeal("No");
		passengerDetail.setName("user");
		passengerDetail.setSeatNumber("1A");
		ticketDetailList.add(ticketDetail);
		  Mockito.when(ticketRepository.findByEmail("user@gmail.com")).thenReturn(ticketDetailList);
		  List<TicketDetail> result=ticketService.getTicketByEmail("user@gmail.com");
		  Assertions.assertSame(result.size(), 1);

	}
	
	@Test
	public void testTicketNotFoundException() throws TicketNotFoundException {
		Assertions.assertThrows(TicketNotFoundException.class, ()->{
			  Mockito.when(ticketRepository.findByEmail("user@gmail.com")).thenThrow(NullPointerException.class);
			  List<TicketDetail> result=ticketService.getTicketByEmail("user12345678@gmail.com");
        });

	}
	
	@Test
	public void testGetAllTicketByEmailAndstatus() throws TicketNotFoundException{
		List<TicketDetail> ticketDetailList = new ArrayList<TicketDetail>();
		TicketDetail ticketDetail=new TicketDetail();
		ticketDetail.setId((long) 1);
		ticketDetail.setEmail("user@gmail.com");
		ticketDetail.setFlightId((long) 58);
		ticketDetail.setFlightName("AirAsia");
		ticketDetail.setPrice((long) 4300);
		ticketDetail.setStatus("active");
		ticketDetail.setDateOfJourney("20-07-2021");
		PassengerDetail passengerDetail=new PassengerDetail();
		passengerDetail.setAge((long) 25);
		passengerDetail.setGender("male");
		passengerDetail.setId((long) 1);
		passengerDetail.setMeal("No");
		passengerDetail.setName("user");
		passengerDetail.setSeatNumber("1A");
		ticketDetailList.add(ticketDetail);
		  Mockito.when(ticketRepository.findByEmailAndStatus("user@gmail.com","active")).thenReturn(ticketDetailList);
		  List<TicketDetail> result=ticketService.getTicketByEmailAndStatus("user@gmail.com","active");
		  Assertions.assertSame(result.size(), 1);

	}
	
	@Test
	public void testTicketByEmailStatusNotFoundException() throws TicketNotFoundException {
		Assertions.assertThrows(TicketNotFoundException.class, ()->{
			  Mockito.when(ticketRepository.findByEmailAndStatus("user@gmail.com","status")).thenThrow(NullPointerException.class);
			  List<TicketDetail> result=ticketService.getTicketByEmailAndStatus("user12345678@gmail.com","active");
        });

	}
	
	@Test
	public void testUpdateTicket() throws TicketNotFoundException{
		Optional<TicketDetail> ticketDetail=Optional.of(new TicketDetail());
		ticketDetail.get().setId((long) 1);
		ticketDetail.get().setEmail("user@gmail.com");
		ticketDetail.get().setFlightId((long) 58);
		ticketDetail.get().setFlightName("AirAsia");
		ticketDetail.get().setPrice((long) 4300);
		ticketDetail.get().setStatus("active");
		ticketDetail.get().setDateOfJourney("20-07-2021");
		PassengerDetail passengerDetail=new PassengerDetail();
		passengerDetail.setAge((long) 25);
		passengerDetail.setGender("male");
		passengerDetail.setId((long) 1);
		passengerDetail.setMeal("No");
		passengerDetail.setName("user");
		passengerDetail.setSeatNumber("1A");
		  Mockito.when(ticketRepository.findById(ticketDetail.get().getId())).thenReturn(ticketDetail);
			 Mockito.when(ticketRepository.save(ticketDetail.get())).thenReturn(ticketDetail.get());
		  TicketDetail result=ticketService.updateTicket(ticketDetail.get());

	}
	
	@Test
	public void testUpdateTicketNotFoundException() throws TicketNotFoundException {
		TicketDetail ticketDetail=new TicketDetail();
		ticketDetail.setId((long) 1);
		ticketDetail.setEmail("user@gmail.com");
		ticketDetail.setFlightId((long) 58);
		ticketDetail.setFlightName("AirAsia");
		ticketDetail.setPrice((long) 4300);
		ticketDetail.setStatus("active");
		ticketDetail.setDateOfJourney("20-07-2021");
		PassengerDetail passengerDetail=new PassengerDetail();
		passengerDetail.setAge((long) 25);
		passengerDetail.setGender("male");
		passengerDetail.setId((long) 1);
		passengerDetail.setMeal("No");
		passengerDetail.setName("user");
		passengerDetail.setSeatNumber("1A");
		Assertions.assertThrows(TicketNotFoundException.class, ()->{
			 Mockito.when(ticketRepository.save(ticketDetail)).thenThrow(NullPointerException.class);
			 TicketDetail result=ticketService.updateTicket(ticketDetail);
        });

	}
	
//	@Test
//	public void testSaveTicket() throws TicketNotFoundException{
//		TicketDetail ticketDetail=new TicketDetail();
//		ticketDetail.setId((long) 1);
//		ticketDetail.setEmail("user@gmail.com");
//		ticketDetail.setFlightId((long) 58);
//		ticketDetail.setFlightName("AirAsia");
//		ticketDetail.setPrice((long) 4300);
//		ticketDetail.setStatus("active");
//		ticketDetail.setDateOfJourney("20-07-2021");
//		PassengerDetail passengerDetail=new PassengerDetail();
//		passengerDetail.setAge((long) 25);
//		passengerDetail.setGender("male");
//		passengerDetail.setId((long) 1);
//		passengerDetail.setMeal("No");
//		passengerDetail.setName("user");
//		passengerDetail.setSeatNumber("1A");
//			 Mockito.when(ticketRepository.save(ticketDetail)).thenReturn(ticketDetail);
//			 Mockito.doNothing().when(ts).updateSeatNumber(ticketDetail);
//		  TicketDetail result=ts.saveTicket(ticketDetail);
//
//	}
	
	@Test
	public void testSaveTicketNotFoundException() throws TicketNotFoundException {
		TicketDetail ticketDetail=new TicketDetail();
		ticketDetail.setId((long) 1);
		ticketDetail.setEmail("user@gmail.com");
		ticketDetail.setFlightId((long) 58);
		ticketDetail.setFlightName("AirAsia");
		ticketDetail.setPrice((long) 4300);
		ticketDetail.setStatus("active");
		ticketDetail.setDateOfJourney("20-07-2021");
		PassengerDetail passengerDetail=new PassengerDetail();
		passengerDetail.setAge((long) 25);
		passengerDetail.setGender("male");
		passengerDetail.setId((long) 1);
		passengerDetail.setMeal("No");
		passengerDetail.setName("user");
		passengerDetail.setSeatNumber("1A");
		Assertions.assertThrows(TicketNotFoundException.class, ()->{
			 Mockito.when(ticketRepository.save(ticketDetail)).thenThrow(NullPointerException.class);
			 TicketDetail result=ticketService.updateTicket(ticketDetail);
        });

	}
	
}
