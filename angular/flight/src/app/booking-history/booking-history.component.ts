import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookingDetailsComponent } from '../book-flight/booking-details/booking-details.component';
import { ManageBookingService } from '../service/manage-bookings.service';
import { BookingHistoryDetail } from '../model/bookHistoryDetail';
import { UserService } from '../service/user-service';

@Component({
  selector: 'app-booking-history',
  templateUrl: './booking-history.component.html',
  styleUrls: ['./booking-history.component.scss']
})
export class BookingHistoryComponent implements OnInit {

  manageBookData:BookingHistoryDetail[]=[];
  userEmail:any;
  constructor(private manageBookService:ManageBookingService,private userService:UserService,public router:Router) { }

  ngOnInit(): void {
    this.userEmail=sessionStorage.getItem('email');
    this.manageBookService.getBooking(this.userEmail).subscribe((response)=>{
      this.manageBookData=response;
    })
  }

  viewDetails(viewDetail:BookingHistoryDetail){
    this.manageBookService.setViewDetail(viewDetail);
    this.router.navigate(['/viewDetail']);
  }

}
