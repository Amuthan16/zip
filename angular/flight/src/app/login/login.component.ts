import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../service/register.service';
import { UserService } from '../service/user-service';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  success: boolean = false;
  fail: boolean = false;

  constructor(private registerService: RegisterService, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
  }


  onSubmit(loginForm: any) {
    this.registerService.validateUser(loginForm.form.controls.email.value, loginForm.form.controls.password.value).subscribe((response:any) => {
      console.log("response", response);
      if (response && response.token) {
        var token = response.token;
        sessionStorage.setItem("token",token);
        let decodedValue:any = jwt_decode(token);
        let u={userName:decodedValue.sub,
          role:decodedValue.role,  
          email:decodedValue.sub,
          valid:true}
      this.userService.setUserValue(u);
      sessionStorage.setItem('role', decodedValue.role);
      sessionStorage.setItem("email",decodedValue.sub);
      sessionStorage.setItem("userName",decodedValue.sub);
      this.registerService.setUserFlag(true);
      this.success=true;
        this.fail=false;
        this.registerService.setEmail(loginForm.form.controls.email.value);
        if(decodedValue.role==='admin'){
          this.router.navigate(['admin/manageAirline']);
        }else{
          this.router.navigate(['bookFlight']);
        }
      }else{
        this.fail=true;
        this.success=false;
      }
    })
  }


}