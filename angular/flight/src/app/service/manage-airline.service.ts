import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ManageAirlineService {

  baseUrl:string="http://ec2-13-58-180-214.us-east-2.compute.amazonaws.com:8989/api/";
  constructor(private http:HttpClient) { }

  addFlight(airlineData:any) {
    return this.http.post<any>(this.baseUrl+'admin/flight',airlineData,
    { observe: 'response' }).pipe(res => {
      return res;
    });
  }
  updateFlight(airlineData:any) {
    let endpoint=this.baseUrl+'admin/flight/'+airlineData.id;
    return this.http.put<any>(endpoint,airlineData,
    { observe: 'response' }).pipe(res => {
      return res;
    });
  }
}
