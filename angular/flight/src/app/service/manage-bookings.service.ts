import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BookingHistoryDetail } from '../model/bookHistoryDetail';

@Injectable({
  providedIn: 'root'
})
export class ManageBookingService {

  baseUrl:string="http://ec2-13-58-180-214.us-east-2.compute.amazonaws.com:8989/api/"
  viewdetailData!: BookingHistoryDetail;
  constructor(private http: HttpClient) { }

  getBooking(email:string) {
    let token=sessionStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({
        token: 'Bearer '+token
      })
    };
    let endpoint=this.baseUrl+'user/ticket?email='+email;
    return this.http.get<any>(endpoint, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

  setViewDetail(value:BookingHistoryDetail){
    this.viewdetailData=value;
  }

  cancelTicket(data:any){
    let endpoint=this.baseUrl+'user/ticket/'+data.id;
    let token=sessionStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({
        token: 'Bearer '+token,
        observe: 'response' ,
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.put<any>(endpoint,data,
      httpOptions).pipe(res => {
        return res;
      });
  }

  getBookingWithoutCancelled(email:string) {
    let endpoint=this.baseUrl+'user/ticketByStatus?email='+email+'&status=active';
    let token=sessionStorage.getItem('token')
    const httpOptions = {
      headers: new HttpHeaders({
        token: 'Bearer '+token
      })
    };
    return this.http.get<any>(endpoint, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}
