import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  baseUrl:string="http://ec2-13-58-180-214.us-east-2.compute.amazonaws.com:8989/api/"

  private userEmail = new BehaviorSubject<String>('');
  userEmailData = this.userEmail.asObservable();
  email:string='';
  private userFlag = new BehaviorSubject<Boolean>(false);
  userFlagData = this.userFlag.asObservable();
  constructor(private http: HttpClient) { }

  getUser(email:string) {
    let endpoint=this.baseUrl+'users?email='+email;
    return this.http.get<any>(endpoint,
    { observe: 'response' }).pipe(res => {
      return res;
    });
  }

  addUser(user:any) {
    return this.http.post<any>(this.baseUrl+'user/register',user,
    { observe: 'response' }).pipe(res => {
      return res;
    });
  }

  validateUser(email: string, password: string ) {
    let data={
      username:email,
      password:password
  }
    const httpOptions = {
      headers: new HttpHeaders({
        observe: 'response' ,
        'Content-Type': 'application/json',
            'Accept':'application/json',
            'Access-Control-Allow-Origin': '*'
      })
    };
    let endpoint=this.baseUrl+'user/authenticate';
    return this.http.post<any>(endpoint,data,
      httpOptions).pipe(res => {
      return res;
    });
  }

  setEmail(email:string){
    this.userEmail.next(email);
  }

  setEmailS(email:string){
    this.email=email;
  }

  setUserFlag(val:boolean){
    this.userFlag.next(val);
  }

}

