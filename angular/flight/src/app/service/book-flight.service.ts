import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BookingDetail } from '../model/bookingDetails';
import { Flight } from '../model/flight';

@Injectable({
  providedIn: 'root'
})
export class BookFlightService {

  baseUrl:string="http://ec2-13-58-180-214.us-east-2.compute.amazonaws.com:8989/api/"

  bookingData:BookingDetail[]=[];
  constructor(private http: HttpClient) { }

  getFlights() {
    let endpoint=this.baseUrl+"admin/flight";
    return this.http.get<any>(endpoint,
    { observe: 'response' }).pipe(res => {
      return res;
    });
  }

  setBookingData(bookFlight:BookingDetail[]){
    this.bookingData=bookFlight;
  }

  updateFlight(bookFlight:Flight){
      let endpoint=this.baseUrl+"admin/flight/"+bookFlight.id;
      return this.http.put<any>(endpoint,bookFlight,
    { observe: 'response' }).pipe(res => {
      return res;
    });
  }
  errorHanler(error:any){
    console.log(error);
    return throwError(error);
  }
  createBookingHistory(bookingData:any){
  let endpoint=this.baseUrl+"user/ticket";
  let token=sessionStorage.getItem('token')
  const httpOptions = {
    headers: new HttpHeaders({
      token: 'Bearer '+token,
      observe: 'response' 
    })
  };
  return this.http.post<any>(endpoint,bookingData, httpOptions)
  .pipe(
    catchError(this.handleError)
  );
  }

  getFlightDetails(id:string){
    let endpoint=this.baseUrl+"admin/flight/"+id;
    return this.http.get<any>(endpoint,
    { observe: 'response' }).pipe(res => {
      return res;
    });
  }

  deleteFlight(id:string){
    let endpoint=this.baseUrl+"admin/flight/"+id;
    return this.http.delete<any>(endpoint,
  { observe: 'response' }).pipe(res => {
    return res;
  });
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
}

