import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ManageDiscountService {

  baseUrl:string="http://ec2-13-58-180-214.us-east-2.compute.amazonaws.com:8989/api/"
  constructor(private httpClient:HttpClient) { }

  createCoupon(couponData:any){
    let endpoint=this.baseUrl+'admin/discount';
    return this.httpClient.post<any>(endpoint,couponData,
  { observe: 'response' }).pipe(res => {
    return res;
  });
  }

  
  getCouponById(id:any){
    let endpoint=this.baseUrl+'admin/discount/'+id;
    return this.httpClient.get<any>(endpoint,
  { observe: 'response' }).pipe(res => {
    return res;
  });
  }

  updateCoupon(id:any,couponData:any){
    let endpoint=this.baseUrl+'admin/discount/'+id;
    return this.httpClient.put<any>(endpoint,couponData,
  { observe: 'response' }).pipe(res => {
    return res;
  });
  }

  getCoupon(){
    let endpoint=this.baseUrl+'admin/discount';
    return this.httpClient.get<any>(endpoint,
  { observe: 'response' }).pipe(res => {
    return res;
  });
  }

  getCouponByCouponCode(code:string){
    let endpoint=this.baseUrl+'admin/discounts?couponCode='+code;
    return this.httpClient.get<any>(endpoint,
  { observe: 'response' }).pipe(res => {
    return res;
  });
  }

  deleteCoupon(id:string){
    let endpoint=this.baseUrl+'admin/discount/'+id;
    return this.httpClient.delete<any>(endpoint,
  { observe: 'response' }).pipe(res => {
    return res;
  });
  }
}
