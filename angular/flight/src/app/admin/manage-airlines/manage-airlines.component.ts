import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookFlightService } from 'src/app/service/book-flight.service';
import { first } from 'rxjs/operators';
import { Flight } from 'src/app/model/flight';

@Component({
  selector: 'app-manage-airlines',
  templateUrl: './manage-airlines.component.html',
  styleUrls: ['./manage-airlines.component.scss']
})
export class ManageAirlinesComponent implements OnInit {
  
  flightData:Flight[]=[];
  constructor(private router:Router,private bookService:BookFlightService) {
      this.bookService.getFlights().subscribe((response)=>{
        this.flightData=response.body;
      })
  }

  ngOnInit(): void {
  }

  
  edit(f:any){
    this.router.navigate(['admin/addEditAirline',f.id]);
  }

  addAirline(){
    this.router.navigate(['admin/addEditAirline']);
  }

  delete(fd:any){
    this.bookService.deleteFlight(fd.id).subscribe((response)=>{
      if(response.body){
        this.bookService.getFlights().subscribe((response)=>{
          this.flightData=response.body;
        })
      }
    })
  }
}
