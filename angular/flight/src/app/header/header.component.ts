import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../service/register.service';
import { UserService } from '../service/user-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userLogin:boolean=false;
  userRole:any;
  userName:any;
  constructor(private router:Router,private registerService:RegisterService,public userservice:UserService) { }

  ngOnInit(): void {
    
    this.registerService.userFlagData.subscribe((response)=>{
      this.userRole=sessionStorage.getItem('role');
      this.userName=sessionStorage.getItem('userName');
      if(this.userRole === 'user'|| 'admin'){
        this.userLogin=true;
      }else{
        this.userLogin=false;
      }
    })
  }

  redirect(){
    this.registerService.setEmail('');
    this.router.navigate(['/login']);
    this.userLogin=false;
    sessionStorage.clear();
    this.registerService.setUserFlag(false);
  }

}
