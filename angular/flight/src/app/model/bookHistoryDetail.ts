import { Flight } from "./flight";

export class BookingHistoryDetail {

    public email:string;
    public flightId:string;
    public flightName:string;
    public dateOfJourney:string;
    public price:number;
    public passengerDetail:any;
    public id:number;
    public status:string;

    constructor( email: string,flightId:string,dateOfJourney:string,price:number,passengerDetail:any,id:number,flightName:string,status:string) { 
        this.email=email;
        this.flightId=flightId;
        this.dateOfJourney=dateOfJourney;
        this.price=price;
        this.passengerDetail=passengerDetail;
        this.id=id;
        this.flightName=flightName;
        this.status=status;
     }

}
