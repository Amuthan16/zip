import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BookFlightService } from '../service/book-flight.service';
import { BookingHistoryDetail } from '../model/bookHistoryDetail';
import { UserService } from '../service/user-service';
import { ManageBookingService } from '../service/manage-bookings.service';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-manage-bookings',
  templateUrl: './manage-bookings.component.html',
  styleUrls: ['./manage-bookings.component.scss']
})
export class ManageBookingsComponent implements OnInit {

  manageBookData: BookingHistoryDetail[] = [];
  url: any;
  email:any;
  constructor(private manageBookService: ManageBookingService, private userService: UserService, private router: Router, private bookFlightService: BookFlightService) { }

  ngOnInit(): void {
    this.email=sessionStorage.getItem('email');
    this.manageBookService.getBookingWithoutCancelled(this.email).subscribe((response) => {
      console.log(response);
      this.manageBookData = response;
    })

  }

  viewDetails(viewDetail: BookingHistoryDetail) {
    this.manageBookService.setViewDetail(viewDetail);
    this.router.navigate(['/viewDetail']);
  }

  cancelTicket(bookingHistory: BookingHistoryDetail) {
    bookingHistory.status='cancelled';
    this.manageBookService.cancelTicket(bookingHistory).subscribe((response) => {
      this.manageBookService.getBookingWithoutCancelled(this.email).subscribe((bookingDataResult) => {
        this.manageBookData = bookingDataResult;
      })
    });
  }

  // click(data: any) {
  //   // let data1="aasA";
  //   this.bookFlightService.getFlightDetails(data.flightId).subscribe((response) => {
  //     let flightData = response.body;
  //     let data1 = '<div id='+data.id+'><h1>Flight Booking Details</h1><div><h5>TicketNumber:</h5><h3>' + data.id + '</h3></div><hr><div><div><h5>From:</h5><h3>' + flightData.fromPlace + '</h3></div><div><h5>TO:</h5><h3>' + flightData.toPlace + '</h3></div></div><div><div><h5>Date:</h5><h3>' + data.dateOfJourney + '</h3></div><div ><h5>Price</h5><h3>Rs.' + data.price + '</h3></div></div><div><div><h5>Start Time:</h5><h3>' + flightData.startDateTime + '</h3> </div><div><h5>End Time</h5><h3>' + flightData.endDateTime + '</h3> </div></div><h2>Passenger Details</h2> <table><thead><tr><th>Passenger Name</th><th>Gender</th><th>Age</th><th>Meal</th><th>Seat Number</th></tr></thead><tbody>';

  //     for(let p of data.passengerDetail){
  //       data1=data1+'<tr><td>'+p.name+ '</td><td>'+p.gender+'</td><td>'+p.age+'</td><td>'+p.meal+'</td><td>'+p.seatNumber+'</td></tr>'
  //     }
  //     data1=data1+'</tbody></table> </div>';
  //     var wrapper= document.createElement('div');
  //     wrapper.innerHTML= data1
  //   var d1 = document.getElementById('table') as HTMLCanvasElement; 
  //     // d1.insertAdjacentHTML('afterend', data1);
  //   var data2 = document.getElementById(data.id) as HTMLCanvasElement;  
  //   html2canvas(data2).then(canvas => {
  //     // Few necessary setting options
  //     var imgWidth = 208;
  //     var pageHeight = 295;
  //     var imgHeight = canvas.height * imgWidth / canvas.width;
  //     var heightLeft = imgHeight;
       
  //     const contentDataURL = canvas.toDataURL('image/png')
  //     let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
  //     var position = 0;
  //     pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
  //     pdf.save('ticket'+data.id+'.pdf'); // Generated PDF
  //     });
  // })
  // }
}
