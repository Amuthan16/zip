import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManageDiscountService } from 'src/app/service/manage-discount.service';
import { Flight } from 'src/app/model/flight';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user-service';
import { BookFlightService } from '../../service/book-flight.service';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.scss']
})
export class BookingDetailsComponent implements OnInit {

  row = [
    {
      name : '',
      gender: 'male',
      age: '',
      meal:'no',
      seatNumber:'',
      returnSeatNumber:''
    }
  ]
  bookingData!: Flight;
  totalPrice:number=0;
  returnBookingData:any;
  bookFlag:boolean=false;
  couponCode:string='';
  discountFlag=false;
  discountedAmount:number=0;
  disAmntDepart:number=0;
  disAmntReturn:number=0
  departSeats:string[]=[];
  returnSeats:string[]=[];
  userEmail:any;
  invalidCoupon: boolean =false;
  constructor(public bookService:BookFlightService,private userService:UserService,private couponservice:ManageDiscountService,
    private router:Router) { }

  ngOnInit(): void {
    this.bookingData=this.bookService.bookingData[0].bookFlight;
    this.createSeatNumber();
    if(this.bookService.bookingData.length==2){
      this.returnBookingData=this.bookService.bookingData[1].bookFlight;
      this.createReturnSeatNumber();  
    }
    this.userEmail=sessionStorage.getItem('email');
  }
  createReturnSeatNumber() {
    let count=0;
   let rows=Math.round(this.returnBookingData.totalNoOfSeats/this.returnBookingData.noOfRows)
    for(let i=1;i<=this.returnBookingData.totalNoOfSeats;i++){
      if(count<=this.returnBookingData.totalNoOfSeats){
        for(let j=0;j<rows;j++){
          this.returnSeats.push(i+ String.fromCharCode(65 + j))
          count=count+1;
        }
      }else{
        break;
      }
     
    }
  }
  createSeatNumber() {
   let count=0;
   let rows=Math.round(this.bookingData.totalNoOfSeats/this.bookingData.noOfRows)
    for(let i=1;i<=this.bookingData.totalNoOfSeats;i++){
      if(count<=this.bookingData.totalNoOfSeats){
        for(let j=0;j<rows;j++){
          this.departSeats.push(i+ String.fromCharCode(65 + j))
          count=count+1;
        }
      }else{
        break;
      }
     
    }
  }
 
  
  addTable() {
    const obj = {
      name: '',
      gender: 'male',
      age: '',
      meal:'no',
      seatNumber:'',
      returnSeatNumber:''
    }
    this.row.push(obj)
  }
  
  deleteRow(x:any){
    var delBtn = confirm(" Do you want to delete ?");
    if ( delBtn == true ) {
      this.row.splice(x, 1 );
    }   
  } 

  confirmBooking(){ 
    let data;
    if(this.bookService.bookingData.length==2){
      let departFlightCount= this.bookingData;
      departFlightCount.totalNoOfSeats=this.bookingData.totalNoOfSeats-this.row.length;
      this.bookService.updateFlight(departFlightCount).subscribe((response)=>{
        if(response){
          let returnFlight= this.returnBookingData;
          returnFlight.totalNoOfSeats=this.returnBookingData.totalNoOfSeats-this.row.length;
          this.bookService.updateFlight(returnFlight).subscribe((response)=>{
            let departData={
              email:this.userEmail,
              flightId:this.bookingData.id,
              flightName:this.bookingData.airlineName,
              dateOfJourney:this.bookService.bookingData[0].departDate,
              price:this.bookingData.price*this.row.length,
              status:'active',
              passengerDetail:this.row
            }
            let returnData={
              email:this.userEmail,
              flightId:this.returnBookingData.id,
              flightName:this.returnBookingData.airlineName,
              dateOfJourney:this.bookService.bookingData[1].returnDate,
              price:this.returnBookingData.price*this.row.length,
              status:'active',
              passengerDetail:this.row
            }
            if(this.discountFlag){
              departData.price=this.disAmntDepart;
              returnData.price=this.disAmntReturn;
            }
            this.bookService.createBookingHistory(departData).subscribe((response:any)=>{
              this.bookService.createBookingHistory(returnData).subscribe((response:any)=>{
                this.afterBooking();
              })
            })
          })
        }
      })
     
  }else{
    let dataFlight= this.bookingData;
    dataFlight.totalNoOfSeats=this.bookingData.totalNoOfSeats-this.row.length;
    this.bookService.updateFlight(dataFlight).subscribe((response)=>{
      data={
        email:this.userEmail,
        flightId:this.bookingData.id,
        flightName:this.bookingData.airlineName,
        dateOfJourney:this.bookService.bookingData[0].departDate,
        price:this.bookingData.price*this.row.length,
        status:'active',
        passengerDetail:this.row
      }
      if(this.discountFlag){
        data.price=this.discountedAmount;
      }
      this.bookService.createBookingHistory(data).subscribe((response:any)=>{
       this.afterBooking();
      })
      
    });
     
  }
  }

  afterBooking(){
    this.bookFlag=true;
   this.row = [
      {
        name : '',
        gender: 'male',
        age: '',
        meal:'no',
        seatNumber:'',
        returnSeatNumber:''
      }
    ]
    this.couponCode='';
    setTimeout(() =>{
     this.router.navigate(['manageBooking'])
   }, 5000);
  }

  checkCoupon(){
    this.couponservice.getCouponByCouponCode(this.couponCode).subscribe((response:any)=>{
      console.log(response)
      if(response && response.body && response.body.maxAmount){
        console.log(response.id)
        let maxAmount=response.body.maxAmount;
        let percentage=response.body.percentage;
        let price;
        if(this.bookService.bookingData.length==1){
          price=this.bookingData.price*this.row.length;
        }else{
          price=(this.bookingData.price*this.row.length)+(this.returnBookingData.price*this.row.length);
        }
        let maxDis=price-maxAmount;
        let maxPercentage=price-((price/100)*percentage);
        this.discountFlag=true;
        this.invalidCoupon=false;
        if(maxDis>maxPercentage){
          this.discountedAmount=maxDis;
          this.disAmntDepart=this.bookingData.price*this.row.length-maxAmount/2;
          if(this.bookService.bookingData.length===2){
            this.disAmntReturn=this.returnBookingData.price*this.row.length-maxAmount/2;
          }
         
        }else{
          this.discountedAmount=maxPercentage;
          this.disAmntDepart=this.bookingData.price*this.row.length-((price/100)*percentage)/2;
          if(this.bookService.bookingData.length===2){
            this.disAmntReturn=this.returnBookingData.price*this.row.length-((price/100)*percentage)/2;
          }
          
        }
        
      }else{
        this.discountFlag=false;
        this.invalidCoupon=true;
      }
    })
  }

}
