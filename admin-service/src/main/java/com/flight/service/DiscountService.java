package com.flight.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flight.entity.Discount;
import com.flight.exception.DiscountNotFoundException;
import com.flight.repository.DiscountRepository;

@Service
public class DiscountService {

	@Autowired
	private DiscountRepository discountRepository;

	public Discount saveDiscount(Discount discount) {
		// TODO Auto-generated method stub
		return discountRepository.save(discount);
	}

	public Discount updateDiscount(Discount discount, Long id) throws DiscountNotFoundException {
		// TODO Auto-generated method stub
		Optional<Discount> discountVal=discountRepository.findById(id);
		if(discountVal.isPresent()) {
			return discountRepository.save(discount);
		}
		else {
			throw new DiscountNotFoundException("No Discount found with this id "+id);
		}
		
	}

	public List<Discount> getAllDiscount() {
		// TODO Auto-generated method stub
		return discountRepository.findAll();
	}

	public Discount getDiscountById(Long id) throws DiscountNotFoundException {
		// TODO Auto-generated method stub
		Optional<Discount> discount=discountRepository.findById(id);
		if(discount.isPresent()) {
			return discount.get();
		}
		else {
			throw new DiscountNotFoundException("No Discount found with this id "+id);
		}
	}

	public Boolean deleteDiscount(Long id) throws DiscountNotFoundException {
		// TODO Auto-generated method stub
		if (discountRepository.findById(id).isPresent()) {
			discountRepository.deleteById(id);
			return true;
		}
		else {
			throw new DiscountNotFoundException("No Discount found with this id "+id);
		}
	}

	public Discount getDiscountByCouponCode(String couponCode) throws DiscountNotFoundException {
		// TODO Auto-generated method stub
		List<Discount> discountList=discountRepository.findByCouponCode(couponCode);
		if(discountList.size()>0) {
			return discountList.get(0);
		}else {
			throw new DiscountNotFoundException("No Discount found with this coupon code "+couponCode);
		}
	}

}
