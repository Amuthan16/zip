package com.flight.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.flight.entity.Airline;
import com.flight.entity.Flight;
import com.flight.exception.FlightNotFoundException;
import com.flight.repository.FlightRepository;

@Service
public class FlightService {
	
	@Autowired
	private FlightRepository flightRepository;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	private static final String TOPIC="flightTopic";
	
	public Airline getFlightById(Long id) throws FlightNotFoundException {
		// TODO Auto-generated method stub
		Optional<Flight> flight=flightRepository.findById(id);
		if(flight.isPresent()) {
			return convertToAirline(flight.get());
		}
		else {
			throw new FlightNotFoundException("No flight found with this id "+id);
		}
	}

	public List<Airline> getAllFlight() {
		// TODO Auto-generated method stub
		List<Flight> flightAll=flightRepository.findAll();
		List<Airline> airlineList=new ArrayList<Airline>();
		for(Flight flight:flightAll) {
			airlineList.add(convertToAirline(flight));
		}
		return airlineList;
	}

	public Airline updateAirline(Airline airline, Long id) throws FlightNotFoundException {
		// TODO Auto-generated method stub
		Optional<Flight> existFlight=flightRepository.findById(id);
		if(existFlight.isPresent()) {
			Flight flight =convertToFlight(airline);
			Flight flightData=flightRepository.save(flight);
//			if(airline.getBlock().equals("blocked")) {
//				kafkaTemplate.send(TOPIC,flightData.getId().toString());
//			}
			
			return convertToAirline(flightData);
		}
		else {
			throw new FlightNotFoundException("No flight found with this id "+id);
		}
	
		
		
		
	}

	public Airline savAirline(Airline airline) {
		// TODO Auto-generated method stub
		Flight flight =convertToFlight(airline);
		Flight flightData=flightRepository.save(flight);
		
		
		return convertToAirline(flightData);
	}
	
	public Flight convertToFlight(Airline airline) {
		String scheduledDay=String.join(",", airline.getScheduledDays());
		String meal=String.join(",", airline.getMeal());
		Flight flight =new Flight();
		if(airline.getId()!=null) {
			flight.setId(airline.getId());
		}
		flight.setEmailId(airline.getEmailId());
		flight.setPhoneNumber(airline.getPhoneNumber());
		flight.setFlightNumber(airline.getFlightNumber());
		flight.setBlock(airline.getBlock());
		flight.setAirlineName(airline.getAirlineName());
		flight.setFromPlace(airline.getFromPlace());
		flight.setToPlace(airline.getToPlace());
		flight.setStartDateTime(airline.getStartDateTime());
		flight.setEndDateTime(airline.getEndDateTime());
		flight.setScheduledDays(scheduledDay);
		flight.setInstrumentUsed(airline.getInstrumentUsed());
		flight.setTotalNoOfSeats(airline.getTotalNoOfSeats());
		flight.setTotalNoOfBussinessSeats(airline.getTotalNoOfBussinessSeats());
		flight.setPrice(airline.getPrice());
		flight.setNoOfRows(airline.getNoOfRows());
		flight.setMeal(meal);
		return flight;
	}
	
	public Airline convertToAirline(Flight flight) {
		System.out.println(flight.getMeal());
		 List <String> mealList = Arrays.asList(flight.getMeal().split(","));
		List<String> scheduledDayList =Arrays.asList(flight.getScheduledDays().split(",")); 
		Airline airline =new Airline();
		airline.setId(flight.getId());
		airline.setEmailId(flight.getEmailId());
		airline.setPhoneNumber(flight.getPhoneNumber());
		airline.setFlightNumber(flight.getFlightNumber());
		airline.setBlock(flight.getBlock());
		airline.setAirlineName(flight.getAirlineName());
		airline.setFromPlace(flight.getFromPlace());
		airline.setToPlace(flight.getToPlace());
		airline.setStartDateTime(flight.getStartDateTime());
		airline.setEndDateTime(flight.getEndDateTime());
		airline.setScheduledDays(scheduledDayList);
		airline.setInstrumentUsed(flight.getInstrumentUsed());
		airline.setTotalNoOfSeats(flight.getTotalNoOfSeats());
		airline.setTotalNoOfBussinessSeats(flight.getTotalNoOfBussinessSeats());
		airline.setPrice(flight.getPrice());
		airline.setNoOfRows(flight.getNoOfRows());
		airline.setMeal(mealList);
		return airline;
	}

	public Boolean deleteFlight(Long id) throws FlightNotFoundException {
		if (flightRepository.findById(id).isPresent()) {
			flightRepository.deleteById(id);
			return true;
		}
		else {
			throw new FlightNotFoundException("No flight found with this id "+id);
		}
	}

	

}
