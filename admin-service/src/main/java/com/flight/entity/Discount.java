package com.flight.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="discount")
public class Discount {
	  

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String couponCode;
    private int percentage;
    private Long maxAmount;
    private String startDate;
    private String endDate;
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public int getPercentage() {
		return percentage;
	}
	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}
	public Long getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(Long maxAmount) {
		this.maxAmount = maxAmount;
	}
	public Long getId() {
		return id;
	}
	
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * @param id
	 * @param couponCode
	 * @param percentage
	 * @param maxAmount
	 * @param startDate
	 * @param endDate
	 */
	public Discount(Long id, String couponCode, int percentage, Long maxAmount, String startDate, String endDate) {
		super();
		this.id = id;
		this.couponCode = couponCode;
		this.percentage = percentage;
		this.maxAmount = maxAmount;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	public Discount() {}
	@Override
	public String toString() {
		return "Discount [id=" + id + ", couponCode=" + couponCode + ", percentage=" + percentage + ", maxAmount="
				+ maxAmount + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}
    
}
