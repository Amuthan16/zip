package com.flight.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name="airline")
public class Flight {
	

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String emailId;
    private Long phoneNumber;
    private String flightNumber;
    private String block;
    private String airlineName;
    private String fromPlace;
    private String toPlace;
    private String startDateTime;
    private String endDateTime;
    private String scheduledDays;
    private String instrumentUsed;
    private long totalNoOfSeats;
    private long totalNoOfBussinessSeats;
    private long price;
    private long noOfRows;
    private String meal;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the phoneNumber
	 */
	public Long getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}
	/**
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	/**
	 * @return the block
	 */
	public String getBlock() {
		return block;
	}
	/**
	 * @param block the block to set
	 */
	public void setBlock(String block) {
		this.block = block;
	}
	/**
	 * @return the airlineName
	 */
	public String getAirlineName() {
		return airlineName;
	}
	/**
	 * @param airlineName the airlineName to set
	 */
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	/**
	 * @return the fromPlace
	 */
	public String getFromPlace() {
		return fromPlace;
	}
	/**
	 * @param fromPlace the fromPlace to set
	 */
	public void setFromPlace(String fromPlace) {
		this.fromPlace = fromPlace;
	}
	/**
	 * @return the toPlace
	 */
	public String getToPlace() {
		return toPlace;
	}
	/**
	 * @param toPlace the toPlace to set
	 */
	public void setToPlace(String toPlace) {
		this.toPlace = toPlace;
	}
	/**
	 * @return the startDateTime
	 */
	public String getStartDateTime() {
		return startDateTime;
	}
	/**
	 * @param startDateTime the startDateTime to set
	 */
	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}
	/**
	 * @return the endDateTime
	 */
	public String getEndDateTime() {
		return endDateTime;
	}
	/**
	 * @param endDateTime the endDateTime to set
	 */
	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}
	/**
	 * @return the scheduledDays
	 */
	public String getScheduledDays() {
		return scheduledDays;
	}
	/**
	 * @param scheduledDays the scheduledDays to set
	 */
	public void setScheduledDays(String scheduledDays) {
		this.scheduledDays = scheduledDays;
	}
	/**
	 * @return the instrumentUsed
	 */
	public String getInstrumentUsed() {
		return instrumentUsed;
	}
	/**
	 * @param instrumentUsed the instrumentUsed to set
	 */
	public void setInstrumentUsed(String instrumentUsed) {
		this.instrumentUsed = instrumentUsed;
	}
	/**
	 * @return the totalNoOfSeats
	 */
	public long getTotalNoOfSeats() {
		return totalNoOfSeats;
	}
	/**
	 * @param totalNoOfSeats the totalNoOfSeats to set
	 */
	public void setTotalNoOfSeats(long totalNoOfSeats) {
		this.totalNoOfSeats = totalNoOfSeats;
	}
	/**
	 * @return the totalNoOfBussinessSeats
	 */
	public long getTotalNoOfBussinessSeats() {
		return totalNoOfBussinessSeats;
	}
	/**
	 * @param totalNoOfBussinessSeats the totalNoOfBussinessSeats to set
	 */
	public void setTotalNoOfBussinessSeats(long totalNoOfBussinessSeats) {
		this.totalNoOfBussinessSeats = totalNoOfBussinessSeats;
	}
	/**
	 * @return the price
	 */
	public long getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(long price) {
		this.price = price;
	}
	/**
	 * @return the noOfRows
	 */
	public long getNoOfRows() {
		return noOfRows;
	}
	/**
	 * @param noOfRows the noOfRows to set
	 */
	public void setNoOfRows(long noOfRows) {
		this.noOfRows = noOfRows;
	}
	/**
	 * @return the meal
	 */
	public String getMeal() {
		return meal;
	}
	/**
	 * @param meal the meal to set
	 */
	public void setMeal(String meal) {
		this.meal = meal;
	}
	/**
	 * @param id
	 * @param emailId
	 * @param phoneNumber
	 * @param flightNumber
	 * @param block
	 * @param airlineName
	 * @param fromPlace
	 * @param toPlace
	 * @param startDateTime
	 * @param endDateTime
	 * @param scheduledDays
	 * @param instrumentUsed
	 * @param totalNoOfSeats
	 * @param totalNoOfBussinessSeats
	 * @param price
	 * @param noOfRows
	 * @param meal
	 */
	public Flight(Long id, String emailId, Long phoneNumber, String flightNumber, String block, String airlineName,
			String fromPlace, String toPlace, String startDateTime, String endDateTime, String scheduledDays,
			String instrumentUsed, long totalNoOfSeats, long totalNoOfBussinessSeats, long price, long noOfRows,
			String meal) {
		super();
		this.id = id;
		this.emailId = emailId;
		this.phoneNumber = phoneNumber;
		this.flightNumber = flightNumber;
		this.block = block;
		this.airlineName = airlineName;
		this.fromPlace = fromPlace;
		this.toPlace = toPlace;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.scheduledDays = scheduledDays;
		this.instrumentUsed = instrumentUsed;
		this.totalNoOfSeats = totalNoOfSeats;
		this.totalNoOfBussinessSeats = totalNoOfBussinessSeats;
		this.price = price;
		this.noOfRows = noOfRows;
		this.meal = meal;
	}
    
	public Flight() {}

}
