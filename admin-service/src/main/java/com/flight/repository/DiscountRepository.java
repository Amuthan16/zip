package com.flight.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flight.entity.Discount;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, Long>{

	List<Discount> findByCouponCode(String couponCode);
	
	

}
