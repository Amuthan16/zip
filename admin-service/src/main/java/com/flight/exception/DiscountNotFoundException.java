package com.flight.exception;

public class DiscountNotFoundException extends Exception{
	public DiscountNotFoundException() {}
	public DiscountNotFoundException(String m) {
		super(m);
	}
	public DiscountNotFoundException(Exception e) {
		super(e);
	}
	public DiscountNotFoundException(String m, Exception e) {
		super(m, e);
	}
}
