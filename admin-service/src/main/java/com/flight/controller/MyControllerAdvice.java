package com.flight.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.flight.exception.DiscountNotFoundException;
import com.flight.exception.FlightNotFoundException;
import com.flight.model.ExceptionMessage;

@ControllerAdvice
public class MyControllerAdvice {

	@ExceptionHandler(FlightNotFoundException.class)
	public ResponseEntity<ExceptionMessage> handleUserAlreadyFoundExceptionException(FlightNotFoundException e) {
		ExceptionMessage obj = new ExceptionMessage(e.getMessage(), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<ExceptionMessage>(obj, HttpStatus.OK);
	}
	
	@ExceptionHandler(DiscountNotFoundException.class)
	public ResponseEntity<ExceptionMessage> handleTicketNotFoundExceptionException(DiscountNotFoundException e) {
		ExceptionMessage obj = new ExceptionMessage(e.getMessage(), HttpStatus.BAD_REQUEST);
		return new ResponseEntity<ExceptionMessage>(obj, HttpStatus.OK);
	}
}
