package com.flight.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.flight.entity.Airline;
import com.flight.exception.FlightNotFoundException;
import com.flight.service.FlightService;

@RestController
@CrossOrigin
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	@PostMapping("/flight")
	public ResponseEntity<Airline> createAirline(@RequestBody Airline airline) {
		return new ResponseEntity<Airline>( flightService.savAirline(airline),HttpStatus.OK);
	}

	@PutMapping("/flight/{id}")
	public ResponseEntity<Airline> updateAirline(@RequestBody Airline airline, @PathVariable Long id) throws FlightNotFoundException {

		return new ResponseEntity<Airline>( flightService.updateAirline(airline,id),HttpStatus.OK);
	}
	
	@GetMapping("/flight")
	public ResponseEntity<List<Airline>> getAllFlight() {
		return new ResponseEntity<List<Airline>>(flightService.getAllFlight(),HttpStatus.OK);
	}
	
	
	@GetMapping("/flight/{id}")
	public ResponseEntity<Airline> getFlight( @PathVariable Long id) throws FlightNotFoundException {
		System.out.println("inside get by id");
		return new ResponseEntity<Airline>( flightService.getFlightById(id),HttpStatus.OK);
	}
	
	@DeleteMapping("/flight/{id}")
	public ResponseEntity<Boolean> deleteFlight( @PathVariable Long id) throws FlightNotFoundException {
		
		return new ResponseEntity<Boolean>( flightService.deleteFlight(id),HttpStatus.OK);
	}
}
