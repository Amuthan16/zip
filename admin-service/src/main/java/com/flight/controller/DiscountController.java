package com.flight.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flight.entity.Discount;
import com.flight.exception.DiscountNotFoundException;
import com.flight.service.DiscountService;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
public class DiscountController {
	
	@Autowired
	private  DiscountService discountService;
	
	@Autowired
	private CacheManager cacheManager;

	@PostMapping("/discount")
	@CacheEvict(value = "discount", allEntries=true)
	public ResponseEntity<Discount> createDiscount(@RequestBody Discount discount) {
		return new ResponseEntity<Discount>( discountService.saveDiscount(discount),HttpStatus.OK);
	}

	@PutMapping("/discount/{id}")
	@CacheEvict(key="#id", value = "discount")
	public ResponseEntity<Discount> updateDiscount(@RequestBody Discount discount, @PathVariable Long id) throws DiscountNotFoundException {

		return new ResponseEntity<Discount>( discountService.updateDiscount(discount,id),HttpStatus.OK);
	}
	
	@GetMapping("/discount")
	public ResponseEntity<List<Discount>> getAllDiscount() {
		return new ResponseEntity<List<Discount>>( discountService.getAllDiscount(),HttpStatus.OK);
	}
	
	@ApiOperation(
			value="Find discount by id",
			notes="Provide positive number as id",
			response = Discount.class
			)
	@GetMapping("/discount/{id}")
	@Cacheable(key="#id", value = "discount")
	public ResponseEntity<Discount>  getDiscount( @PathVariable Long id) throws DiscountNotFoundException {
		System.out.println("inside get by id");
		return new ResponseEntity<Discount>( discountService.getDiscountById(id),HttpStatus.OK);
	}
	
	@DeleteMapping("/discount/{id}")
	@CacheEvict(key="#id", value = "discount")
	public ResponseEntity<Boolean>  deleteDiscount( @PathVariable Long id) throws DiscountNotFoundException {
		
		return new ResponseEntity<Boolean>( discountService.deleteDiscount(id),HttpStatus.OK);
	}
	
	@GetMapping("/discounts")
	public ResponseEntity<Discount>  getDiscountByCouponCode( @RequestParam String couponCode) throws DiscountNotFoundException {
		return new ResponseEntity<Discount>( discountService.getDiscountByCouponCode(couponCode),HttpStatus.OK);
	}
	
	@Scheduled(fixedRate = 2000*60)
	public void evictAllcachesAtIntervals() {
	    evictAllCaches();
	}
	
	public void evictAllCaches() {
		System.out.println("inside the cache");
        cacheManager.getCacheNames().stream()
          .forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }
}
